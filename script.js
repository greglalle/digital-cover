/**
 * Animation au clic sur un volet
 */
var views = document.getElementsByClassName("single-view");

for (var i = 0; i < views.length; i++) {
    views[i].addEventListener('click', function() {
            
        for (var i = 0; i < views.length; i++) {
            views[i].classList.remove("active");
        }
        this.classList.add("active");

    });
}

/**
 * Fonction permettant de découper un texte en mots et en lignes
 */
function splitLines(element) {
    if( element == undefined )
        return false;

    var text = element;
    var e = text.innerHTML.split(" ");
    var i = 0;
    var n = 0;

    text.innerHTML = "";

    for (var r = 0; r < e.length; r++) {
        var o = e[r];

        if ("" !== o) {
            var word = document.createElement("span");
            word.classList.add("word");
            word.innerHTML = o + " ";

            text.appendChild(word);

            if (word.offsetTop > i) {
                i = word.offsetTop;
                n++;
            }

            word.classList.add("line-" + n);
            word.style.transitionDelay = 100 * n + "ms";
        }
    }
}

var text = document.querySelectorAll("p");

for (var i=0 ; i < text.length ; i++) {
    splitLines(text[i]);
}

/**
 * Burger menu animation
 */

(function() {
    var burgerMenu = document.getElementsByClassName('burger-menu')[0];
    var menu = document.getElementsByClassName('menu')[0];

    TweenMax.set('.menu-link > .w-o > .o > .link-letter', 1, {yPercent: '75%', autoAlpha:0})

  
    burgerMenu.addEventListener('click', function toggleClasses() {
        [burgerMenu, menu].forEach(function (el) {

            if(el.classList.contains('open')) {
                setTimeout(function() {
                    el.classList.remove('open');
                }, 1500)
                TweenMax.staggerFromTo('.menu-link > .w-o > .o > .link-letter', 1, {yPercent: 0, autoAlpha:1}, {yPercent: '75%', autoAlpha:0, ease:Expo.easeInOut}, 0.02)
            } else {
                el.classList.add('open');
                TweenMax.staggerFromTo('.menu-link > .w-o > .o > .link-letter', 1, {yPercent: '75%', autoAlpha:0}, {yPercent: 0, autoAlpha:1, ease:Expo.easeInOut}, 0.02)

            }

        });
    }, false);
})();

/**
 * Fonction permettant de découper un mot en lettres
 */
function splitLetters(element, letterClass) {
    if( element == undefined )
        return false;

    var text = element.innerHTML;
    var html = '';

    if( letterClass == undefined)
        var className = "c";
    else
        var className = letterClass;

    text.split(' ').forEach(function(w) {
        html += '<span class="w-o">';

        w.split('').forEach(function(c){
            html += '<span class="o"><span class=' + className + ' ref="Letters">' + c + '</span></span>';
        });

        html += '</span>';
    });

    element.innerHTML = html;

}

var menuLinks = document.getElementsByClassName("menu-link");

for (var i=0 ; i < menuLinks.length ; i++) {
    splitLetters(menuLinks[i], "link-letter")
}